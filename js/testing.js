describe('betslipCtrl', function () {
    var $controller;

    beforeEach(function () {
        module('tipicoApp');
    });

    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
    }));

    it('Initialises variables.', inject(function ($controller) {
        var $scope = {};

        var ctrl = $controller('betslipCtrl', { $scope: $scope, });

        $scope.Init();
        expect($scope.balance).toEqual(325.50);
        expect($scope.decimalPressed).toEqual(false);
        expect($scope.dotZero).toEqual(false);

    }));

    it('Checks keypress of clear all in Keypad.', inject(function ($controller) {
        var $scope = {};
        var $rootScope = {};
        var ctrl = $controller('betslipCtrl', { $scope: $scope, $rootScope: $rootScope });

        $scope.Keypress('x');
        expect($rootScope.wager).toEqual(0);

    }));

    it('Checks that a bet is removed from the betslip.', inject(function ($controller) {
        var $scope = {};
        var $rootScope = {};
        var ctrl = $controller('betslipCtrl', { $scope: $scope, $rootScope: $rootScope });

        var odd = '{"id":16646752810,"name":"X","value":"5,00","status":null,"selected":false,"$$hashKey":"object:47"}';
        odd = JSON.parse(odd);

        var oldTotalBets = $rootScope.totalBets;
        var oldTotalStake = $rootScope.totalStake;

        $scope.RemoveBet(odd);

        expect($rootScope.totalBets).toEqual(oldTotalBets - 1);
        expect($rootScope.totalStake).toEqual(oldTotalStake - parseFloat(odd.value));

    }));


});
