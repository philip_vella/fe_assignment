var tipicoApp = angular.module('tipicoApp', ['live.event.service']);


tipicoApp.controller('tipicoCtrl', function ($scope) {
    $scope.OpenRightMenu = function () {
        document.body.dataset.active = document.body.dataset.active == 'main' ? 'betslip' : 'main';
    };

    $scope.BackToMain = function () {
        document.body.dataset.active = 'main';
    }

    //$scope.OpenRightMenu();

});
tipicoApp.controller('betslipCtrl', function ($scope, liveEventService, $timeout, $rootScope) {


    $scope.Init = function () {
        $scope.balance = 325.50;
        $scope.decimalPressed = false;
        $scope.dotZero = false;
    };

    $scope.Init();

    $scope.PlaceBet = function () {
        if ($rootScope.totalBets > 0) {
            $scope.ClearBets();
            var submitted = document.getElementById('betslip-submitted');

            submitted.dataset.active = 'true';

            $timeout(function () {
                submitted.dataset.active = 'false';
            }, 3000);
        }
    }

    $scope.ClearBets = function () {
        if ($rootScope.totalBets > 0) {
            angular.forEach($rootScope.EventDetails.odds, function (odd, key) {
                angular.forEach(odd.results, function (result, key) {
                    result.selected = false;
                });
            });

            $rootScope.totalBets = 0;
        }
    }

    $scope.Keypress = function (key) {

        $rootScope.wager = typeof $rootScope.wager == 'undefined' ? 0 : $rootScope.wager;

        var wager = $rootScope.wager.toString();

        switch (key) {
            case 'x':
                {
                    wager = "0.00";
                    $scope.decimalPressed = false;
                }
                break;
            case '.':
                {
                    $scope.decimalPressed = $scope.decimalPressed ? false : true;
                }
                break;
            default: {

                if ($scope.dotZero) {
                    wager = wager + '.0' + key;
                } else if ($scope.decimalPressed & !wager.indexOf('.') > -1) {
                    wager = wager + '.' + key;
                } else {
                    wager = wager + key;
                }

                if (key == '0') {
                    $scope.dotZero = true;
                } else {
                    $scope.dotZero = false;
                }
                $scope.decimalPressed = false;
            }
                break;
        }


        $rootScope.wager = parseFloat(wager);
    };

    $scope.RemoveBet = function (bet) {

        bet.selected = false;
        $rootScope.totalStake = $rootScope.totalStake - parseFloat(bet.value);

        $rootScope.totalBets--;
    };

    $scope.OpenBetMenu = function ($event) {
        var parent = $event.currentTarget.parentElement;
        parent.dataset.active = 'true';

        $timeout(function () {
            parent.dataset.active = 'false';
        }, 3000);

    };

    $scope.OpenKeypad = function () {
        var keypad = document.getElementsByClassName('keypad');
        keypad[0].dataset.active = 'true';
    }

    $scope.CloseKeypad = function () {
        var keypad = document.getElementsByClassName('keypad');
        keypad[0].dataset.active = 'false';
    }

});

tipicoApp.controller('oddsCtrl', function ($scope, liveEventService, $interval, $rootScope, $timeout) {

    $rootScope.totalBets = 0;
    $rootScope.wager = 5.12;
    $rootScope.totalWager = 0;
    //$rootScope.stakeValue = 0;
    $rootScope.totalStake = 0;

    $rootScope.gain = parseFloat($rootScope.totalBets) * parseFloat($rootScope.wager);
    $rootScope.totalWager = $rootScope.totalBets * $rootScope.wager;


    $rootScope.$watchCollection('[totalStake,totalBets,wager]', function (newValue, oldValue) {
        $rootScope.gain = newValue[0] * newValue[1];
        $rootScope.totalWager = newValue[1] * newValue[2];
    });

    /***********************
      populate odds
    ************************/
    liveEventService.getEventDetails().then(function (data) {
        $rootScope.EventDetails = data;
    });

    /*****************************
    set interval for live actions
    ******************************/
    $interval(function () {
        liveEventService.getEventLastAction().then(function (data) {
            $rootScope.EventDetails.lastAction = data;
        });
    }, 4000);

    /*********************
      Add bets to Betslip
    **********************/
    $scope.addToBetslip = function (odd, $event) {
        var oddValue = parseFloat((odd.value).replace(',', '.'));

        if (odd.selected) {
            // REMOVE BET ONLY FROM BETTING SLIP
            // odd.selected = false;
            // $rootScope.totalBets--;
            // $rootScope.totalStake = $rootScope.totalStake - oddValue;
        } else {
            odd.selected = true;
            $rootScope.totalBets++;
            $rootScope.totalStake = $rootScope.totalStake + oddValue;
        }


        var target = $event.currentTarget;
        target.dataset.active = 'true';

        $timeout(function () {
            target.dataset.active = 'false';
        }, 750);

    };

});


