# Assessment Project Comments And Assumptions

Please write down any comments or assumptions you would like us to know when reviewing the test in the list below.

## Comments And Assumptions

A custom Header was used, which is different from the header shown in mockup. This was done because there was no Font-Responsiveness implemented.

When running the Web Application in version 39 of Chrome for Android on Lollipop one will be able to see the 'Tipico Red' in the Browser Toolbar/

A running version of the Application has been uploaded on [http://tipico.vellaphilip.com](http://tipico.vellaphilip.com)

An assumption was made that the Bets are done on only one game which is currently in view.

3 Unit tests have been implemented and can be executed by visiting [http://tipico.vellaphilip.com/jasmine/SpecRunner.php](http://tipico.vellaphilip.com/jasmine/SpecRunner.php)
