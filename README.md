# Assessment Project - HTML, CSS, AngularJS

Welcome to the Tipico Front-End Assessment Project - HTML, CSS, AngularJS

Here you find a short summary of your tasks. Please use this project "fe_assessment" as your skeleton and add new sources to complete the tasks. 
Follow the **Development and Submission steps** in order to do the below **Tasks** and submit them to us for review.

## Tasks (Minimum Requirements)

Create a small mobile app where users are able to:

1. View live bets as shown in the image tipico/live/livetopgame.png (implementation of the live view and statistics tabs are not required)
2. Live pitch events can be found at img/live. The JSON response includes a ```liveevent``` section defining the current live event action, which changes every 4 seconds. User is expected to see the respective live icon matching the live event, as per JSON update. 
3. Kindly use the following JSON as a mocked response from server: http://promo.tipico-news.com/fe-assesment/live-event-service-min.js
4. Live markets in the design are for reference only, kindly refer to the JSON response for the actual live markets.
5. Odds are clickable, and upon selecting, they are expected to be added to the betslip.
6. Betslip will by default be empty - no events. Betslip designs can be found in  tipico/betslip. The Possible gain calculation is (total odds * total stake).
7. SYSTEM + COMBI tabs within the betslip are not required in this assessment. Single tab should always be enabled.
8. Proper client-side error handling + validations
9. Ability to delete bets from the betslip.
10. 'Place Bet' will display a message that the betslip has been submitted.
11. Betslip is accessible from the header's betslip button, and closed via swipe / clicking outside of it.
12. Header shown in mockup is already provided with styling as a starter.
13. Footer of the Live Event page is not required.
14. The keypad inside the betslip does not necessarily need to support decimal numbers for this case, but it would be more realistic if it did.
15. Implement at least 2 unit tests. 

##  Nice to have

These are not required, but will be considered as an asset if done.

1. Implementation of some animation is considered an asset.
2. Responsiveness using flexbox.



## Using the mock data service

We also provided a service which simulates an asynchronous call to a server or api. It is actually an angular
service, returning mock data and selecting a random last action which happened in the game.


#### Using the third party angular service
First, include the third party module in your application, located here:
```
<script type="text/javascript" src="http://promo.tipico-news.com/fe-assesment/live-event-service-min.js"></script>
```


And reference it in your angular module:
```
angular.module('yourApp', ['live.event.service']);
```

To make use of this service simply inject the dependency in your angular component (service, controller, directive etc etc)
```
yourApp.controller('YourController', function (..., liveEventService) {
  //...
```


This service offers two method calls. 
* getEventDetails
* getEventLastAction


The first returns the full event details object which includes the lastAction json property and can be used as follows:
```
liveEventService.getEventDetails().then(function(eventDetailsData){
	//todo - your logic here
});
```

It returns data structured as follows:
```
{
    "id": 148122710,
    "title": "Bayern Munich - Borussia Dortmund",
    "sport": "Football",
    "country": "Germany",
    "version": "25fd0a7d5651a7a5cac1f10e5547b549a1bd59531cc4e866b6c91f31bc2b5fca",
    "odds": [
      {
        "displayName": "3-Way",
        "results": [
          {
            "id": 16646752110,
            "name": "1",
            "value": "1,01",
            "status": null,
            "selected": false
          },
          //... more market result types
        ]
      },
      //...more odds details
    ],
    "bettingAvailable": true,
    "team1": "Bayern Munich",
    "team2": "Borrusia Dortmund",
    "ended": false,
    "livescore": {
      "team1": "3",
      "team2": "1",
      "scoreString": "3:1"
    },
    "liveevent": {
      "action": "goalkick",
      "team": 1
    },
    "matchtime": "38'",
    "lastAction": {
      //populated randomly each time the service is called
    }
}
```

The second function returns the lastAction that happened in this event and can be used as follows:
```
liveEventService.getEventLastAction().then(function(lastAction){
	//todo - your logic here
});
```

The data returns should look like this:
```
{
	"actionType" : "Goal Kick",
	"team" : 1
}
```


## Development and Submission steps

1. Clone the master branch to your local machine
2. Please commit frequently to your local git repository (no need to push)
3. When ready, make sure everything is committed
4. Add any assumptions or notes to the file named COMMENTS.md
5. Compress your solution in ZIP format, upload to a cloud service and send us the link for us to be able to download the solution.


**Good luck!**