<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="theme-color" content="#D0202E" />
    <meta name="msapplication-navbutton-color" content="#D0202E" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <title>Tipico</title>

    <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/styles.css?v=<?php echo date('l jS \of F Y h:i:s A'); ?>" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
</head>
<body lang="en" ng-app="tipicoApp" data-active="main">
    <?php include 'includes/main.php'?>
    <?php include 'includes/betslip.php'?>

    <div class="rotate">
        <img src="/img/icons/rotate.png" style="" />
    </div>

    <script type="text/javascript" src="http://promo.tipico-news.com/fe-assesment/live-event-service-min.js"></script>
    <script type="text/javascript" src="/js/script.js?v=<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
</body>
</html>