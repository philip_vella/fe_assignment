<div class="keypad" data-active="false">
    <div class="close" ng-click="CloseKeypad()">Close</div>
    <div class="row">
        <div ng-click="Keypress('1')" class="number">1</div>
        <div ng-click="Keypress('2')" class="number">2</div>
        <div ng-click="Keypress('3')" class="number">3</div>
    </div>
    <div class="row">
        <div ng-click="Keypress('4')" class="number">4</div>
        <div ng-click="Keypress('5')" class="number">5</div>
        <div ng-click="Keypress('6')" class="number">6</div>
    </div>
    <div class="row">
        <div ng-click="Keypress('7')" class="number">7</div>
        <div ng-click="Keypress('8')" class="number">8</div>
        <div ng-click="Keypress('9')" class="number">9</div>
    </div>
    <div class="row">
        <div ng-click="Keypress('.')" class="number" ng-class="decimalPressed ? 'pressed' : 'not-pressed'">.</div>
        <div ng-click="Keypress('0')" class="number">0</div>
        <div ng-click="Keypress('x')" class="number"><span>X</span></div>
    </div>
</div>