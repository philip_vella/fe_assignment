
<div id="betslip" class="width" ng-controller="betslipCtrl">
    <div>
        <div class="balance">Your account balance is {{balance | currency:"€":2}}</div>
        <div class="tabs">
            <ul>
                <li class="active">Single</li>
                <li>Combi</li>
                <li>System</li>
            </ul>
        </div>
        <div class="no-bets-notif" ng-show="totalBets < 1">
            Your bet slip is empty. Add bets first

            <div id="betslip-submitted">
                <span>
                    <img src="../img/icons/submitted.png" />
                </span>
                <span>Betslip submitted</span>
            </div>
        </div>
        <div class="selectedOdds">
            <ng-cloak ng-repeat="odd in EventDetails.odds">
                <div class="row" ng-repeat="result in odd.results" ng-if="result.selected">

                    <div class="left">

                        <div class="title" title="{{EventDetails.title}}">{{EventDetails.title}}</div>
                        <div class="bet-details">
                            <div class="type">{{odd.displayName}} {{result.name}}</div>
                            <div class="value">{{result.value}}</div>
                        </div>
                    </div>
                    <div class="right">
                        <span class="arrow" ng-click="OpenBetMenu($event)">></span>
                        <span class="remove" ng-click="RemoveBet(result)">Remove</span>
                    </div>

                    <!--<div class="item" ng-click="" ng-repeat="result in odd.results" ng-if="result.selected">
                        <div class="title" title="{{odd.displayName}}">{{odd.displayName}}</div>
                        <div class="name">{{result.name}}</div>
                        <div class="value">{{result.value}}</div>
                    </div>-->

                </div>
            </ng-cloak>
        </div>



        <div class="checkout">
            <?php include 'includes/keypad.php'?>
            <div class="number-of-bets">
                <div class="label">Number of bets</div>
                <div class="amount">{{totalBets}}</div>

            </div>
            <div class="wager">
                <div class="label">Wager per bet</div>
                <div class="input-amount" ng-click="OpenKeypad()">{{wager | currency:"€":2}}</div>
            </div>
            <div class="total-wager">
                <div class="label">Total Wager</div>
                <div class="input-amount">{{wager | currency:"€":2}} x {{totalBets}} = {{totalWager | currency:"€":2}}</div>
            </div>
            <div class="possible-gain">
                <div class="label">Possible gain</div>
                <div class="input-amount">{{gain | currency:"€":2}}</div>
            </div>
            <div class="actions">
                <div ng-click="ClearBets()" class="clear">Clear</div>
                <div ng-click="PlaceBet()" class="place-bet">
                    <span>Place Bet</span>
                    <span>></span>
                </div>
            </div>
        </div>
    </div>
</div>