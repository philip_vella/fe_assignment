<div id="main" class="wrapper animateWidth width" ng-controller="tipicoCtrl">
    <div class="backToMain" data-ng-swipe-right="BackToMain()" ng-click="BackToMain()"></div>
    <header>
        <ul>
            <li class="menu-left" ng-click="OpenLeftMenu()">
                <div class="align-left">
                    <div class="burger-icon"></div>
                </div>
                <div class="align-left">
                    <div class="tipico-icon"></div>
                </div>
            </li>
            <li class="credit">
                <div>
                    <div class="text-wrapper">
                        <div class="sml-text">My Account (€)</div>
                        <div class="big-text">23.175</div>
                    </div>
                </div>
            </li>
            <li class="bets">
                <div class="text-wrapper">
                    <div class="sml-text">My Bets</div>
                    <div class="big-text">239</div>
                </div>
            </li>
            <li class="betting-score">
                <div class="text-wrapper">
                    <div class="sml-text">Betting Slip</div>
                    <div class="big-text">0</div>
                </div>
            </li>
            <li class="menu-right" ng-click="OpenRightMenu()">
                <div class="burger-icon"></div>
            </li>
        </ul>
    </header>
    <div class="content">
        <div class="breadcrumb">
            <ul>
                <li class="breadcrumbBack" ng-click="">
                    <div> <span class="arrow"><</span><span>Back</span></div>
                </li>
                <li><div>Live - Football</div></li>
            </ul>
        </div>
        <div class="tabs">
            <ul>
                <li ng-click=""><div class="active">Odds</div></li>
                <li ng-click=""><div>Live Preview</div></li>
                <li ng-click=""><div>Statistics</div></li>
            </ul>
        </div>
        <div class="oddsctrl-wrapper" ng-controller="oddsCtrl">
            <div class="pitch-container" ng-cloak>
                <div class="overflow"></div>
                <div class="item matchtime">{{EventDetails.matchtime}}</div>
                <div class="item live">live</div>
                <div class="item team1">{{EventDetails.team1}}</div>
                <div class="item team2">{{EventDetails.team2}}</div>
                <div class="item livescore-team1">{{EventDetails.livescore.team1}}</div>
                <div class="item livescore-team2">{{EventDetails.livescore.team2}}</div>
                <div class="item liveevent-action">{{EventDetails.lastAction.actionType}}</div>
                <div class="item liveevent-team">{{EventDetails['team' + EventDetails.lastAction.team]}}</div>

                <img ng-show="'Goal Kick1' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item goalkick-left" src="../img/live/goalkick-left.png" />
                <img ng-show="'Goal Kick2' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item goalkick-right" src="../img/live/goalkick-right.png" />
                <img ng-show="'On Goal1' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item ongoal-left" src="../img/live/ongoal-left.png" />
                <img ng-show="'On Goal2' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item ongoal-right" src="../img/live/ongoal-right.png" />
                <img ng-show="'Penalty1' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item penalty-left" src="../img/live/penalty-left.png" />
                <img ng-show="'Penalty2' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item penalty-right" src="../img/live/penalty-right.png" />
                <img ng-show="'Free Kick1' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item freekick-left" src="../img/live/freekick-left.png" />
                <img ng-show="'Free Kick2' == EventDetails.lastAction.actionType + EventDetails.lastAction.team" class="item freekick-right" src="../img/live/freekick-right.png" />

                <img id="pitch-live" alt="live pitch" src="../img/live/pitch-live.svg" />
            </div>

            <div class="latest-update">
                35' shot next to goal Borussia Dortmund
            </div>

            <div class="odds">

                <div class="row" ng-cloak ng-repeat="odd in EventDetails.odds">
                    <div class="left">
                        <div class="title" title="{{odd.displayName}}">{{odd.displayName}}</div>
                    </div>

                    <div class="right">
                        <div class="item" ng-click="addToBetslip(result, $event)" ng-repeat="result in odd.results">
                            <div class="name">{{result.name}}</div>
                            <div class="value">{{result.value}}</div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>